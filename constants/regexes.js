module.exports = {
    mongoId: /^[a-f\d]{24}$/i,
    tag: /^\S+$/
};