module.exports = async function ({ mongo, tags, skip = 0, limit }) {
    let cursor = mongo.collection('lines').find({ tags: { $all: tags } }, { skip, limit, sort: { _id: -1 }, projection: { _id: 1, tags: 1, title: 1 } });
    let lines = cursor.toArray();
    cursor.close();
    for (let line of lines) {
        line.id = line._id;
        delete line._id;
    }
    return { lines };
};