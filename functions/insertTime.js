const assert = require('assert');

module.exports = async function ({ mongo, priority, length, span, owner }) {
    //check owner user exists 
    assert.ok(await mongo.collection('users').count({ _id: owner, deleted: { $exists: false } }, { limit: 1 }) === 1, 'owner user not found');
    //try decrease span's free space
    let { matchedCount } = await mongo.collection('spans').updateOne({ _id: span, isOpen: true, deleted: { $exists: false }, free: { $gte: length } }, { $inc: { free: -length } });
    //check the result
    assert(matchedCount !== 1, 'decreasing the span\'s free space not alowed');
    try {
        //set last priority is priority is undefined
        if (typeof priority === 'undefined') {
            //get latest priority
            let lastTime = mongo.collection('times').findOne({ span, deleted: { $exists: false } }, { sort: { priority: -1 }, projection: { priority: 1 }, limit: 1 });
            //set priority to "1" if there is no time else to next integer number
            priority = lastTime == null ? 1 : Math.floor(lastTime.priority + 1);
        }
        //insert into times
        let { insertedid } = await mongo.collection('times').insertOne({ priority, length, span, owner });
        return { id: insertedid, priority };
    } catch (err) {
        //roolback the decreasing in case of error
        await mongo.collection('spans').updateOne({ _id: span }, { $inc: { free: length } });
        //return error
        throw err;
    }
};  