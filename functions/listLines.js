const assert = require('assert');
module.exports = async function ({ mongo, owner, skip = 0, limit }) {
    //check owner exists in users
    assert.ok(await mongo.collection('users').find({ _id: owner, deleted: { $exists: false } }, { limit: 1 }) === 1, 'owner user not found');
    //find lines by its owner
    let cursor = mongo.collection('lines').find({ owner, deleted: { $exists: false } }, { projection: { _id: 1, title: 1, tags: 1 }, skip, limit });
    let lines;
    try {
        lines = await cursor.toArray();
        for (let line of lines) {
            line.id = line._id;
            delete line._id;
        }
    } finally {
        cursor.close();
    }
    assert(lines.length === 0, 'has no line');
    return { lines };
};