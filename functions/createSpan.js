const assert = require('assert');
module.exports = async function ({ mongo, line, start, length, title, isOpen = false }) {
    //check line exists in lines
    assert.ok(await mongo.collection('lines').count({ _id: line, deleted: { $exists: false } }, { limit: 1 }) === 1, 'line not found');
    //calc end space of span
    let end = start + length;
    //insert into spans
    let { insertedId } = await mongo.collection('spans').insertOne({ line, start, end, free: length, title, isOpen }, { forceServerObjectId: true, ignoreUndefined: true });
    return { id: insertedId };
};