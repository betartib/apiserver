const assert = require('assert');
module.exports = async function ({ mongo, time, priority }) {
    let { modifiedCount, matchedCount } = await mongo.collection('times').updateOne({ _id: time }, { $set: { priority } });
    assert.ok(matchedCount === 1, 'time not found');
    assert.ok(modifiedCount === 1, 'same priority');
    return {};
};