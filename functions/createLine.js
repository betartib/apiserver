const assert = require('assert');
module.exports = async function ({ mongo, owner, title, tags }) {
    //check line exists in lines
    assert.ok(await mongo.collection('users').count({ _id: owner, deleted: { $exists: false } }, { limit: 1 }) === 1, 'owner user not found');
    //insert into lines
    let { insertedId } = await mongo.collection('spans').insertOne({ owner, title, tags }, { forceServerObjectId: true, ignoreUndefined: true });
    return { id: insertedId };
};