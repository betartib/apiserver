const assert = require('assert');
module.exports = async function ({ mongo, span }) {
    let now = Date.now();
    let { modifiedCount } = await mongo.collection('spans').updateOne({ _id: span, deleted: { $exists: false } }, { $set: { deleted: now } });
    assert.ok(modifiedCount === 1, 'span not found');
    try {
        await mongo.collection('times').updateMany({ span, deleted: { $exists: false } }, { $set: { deleted: now } });
    } catch (err) {
        await mongo.collection('spans').updateOne({ _id: span, deleted: now }, { $unset: { deleted: '' } });
        throw err;
    }
    return {};
};