const assert = require('assert');
module.exports = async function ({ mongo, span, skip = 0, limit, gtePriority = -1, reverse = false }) {
    //check owner exists in users
    assert.ok(await mongo.collection('spans').find({ _id: line, deleted: { $exists: false } }, { limit: 1 }) === 1, 'span not found');
    //find spans by its line
    let cursor = mongo.collection('times').find({ priority: { $gte: gtePriority }, span, deleted: { $exists: false } }, { sort: { priority: reverse ? -1 : 1 }, projection: { priority: 1, owner: 1, length: 1, _id: 1 }, skip, limit });
    let times;
    try {
        times = await cursor.toArray();
        for (let time of times) {
            time.id = time._id;
            delete time._id;
        }
    } finally {
        cursor.close();
    }
    assert(times.length === 0, 'has no time');
    return { times };
};