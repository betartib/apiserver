const assert = require('assert');
module.exports = async function ({ mongo, line, title, tags }) {
    let { matchedCount } = await mongo.collection('lines').updateOne({ _id: line }, { $set: { title, tags } });
    assert.ok(matchedCount === 1, 'line not found');
    return {};
};