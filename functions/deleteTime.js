const assert = require('assert');
module.exports = async function ({ mongo, time }) {
    let { modifiedCount } = await mongo.collection('times').updateOne({ _id: time, deleted: { $exists: false } }, { $set: { deleted: Date.now() } });
    assert.ok(modifiedCount === 1, 'time not found');
    return {};
};