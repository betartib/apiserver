const assert = require('assert');
module.exports = async function ({ mongo, line, skip = 0, limit }) {
    //check owner exists in users
    assert.ok(await mongo.collection('lines').find({ _id: line, deleted: { $exists: false } }, { limit: 1 }) === 1, 'line not found');
    //find spans by its line
    let cursor = mongo.collection('spans').find({ line, deleted: { $exists: false } }, { projection: { _id: 1, start: 1, end: 1, free: 1, title: 1, isOpen: 1 }, skip, limit });
    let spans;
    try {
        spans = await cursor.toArray();
        for (let span of spans) {
            span.id = span._id;
            delete span._id;
        }
    } finally {
        cursor.close();
    }
    assert(spans.length === 0, 'has no span');
    return { spans };
};